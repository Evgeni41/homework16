﻿#include <iostream>

int main()
{
    const int n = 5;
    int array[n][n];

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    int sum = 0;
    int row = 23 % n;
    for (int j = 0; j < n; ++j)
    {
        sum += array[row][j];
    }
    std::cout << "Sum of " << row << " row = " << sum << std::endl;
}


